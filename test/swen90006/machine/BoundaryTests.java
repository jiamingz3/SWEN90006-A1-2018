package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class BoundaryTests
{
  //Any method annotated with "@Before" will be executed before each test,
  //allowing the tester to set up some shared resources.
  @Before public void setUp()
  {
  }

  //Any method annotated with "@After" will be executed after each test,
  //allowing the tester to release any shared resources used in the setup.
  @After public void tearDown()
  {
  }

  //To test B1 in Machine.
  @Test public void b1Test()
  {
    final InvalidInstructionException expected = new InvalidInstructionException();
    final List<String> instructions = new ArrayList<>();
    instructions.add("R0");
    Machine m = new Machine();
    try {
      m.execute(instructions);
    }
    catch (InvalidInstructionException e){
      assertSame(expected.toString(), e.toString());
      return;
    }
    assertEquals(expected.toString(), m.execute(instructions));
  }

  //To test B5 in Machine.
  @Test public void b5Test()
  {
    final InvalidInstructionException expected = new InvalidInstructionException();
    final List<String> instructions = new ArrayList<>();
    instructions.add("RET R32");
    Machine m = new Machine();
    try {
      m.execute(instructions);
    }
    catch (InvalidInstructionException e){
      assertSame(expected.toString(), e.toString());
      return;
    }
    assertEquals(expected.toString(), m.execute(instructions));
  }

  //To test B6 in Machine.
  @Test public void b6Test()
  {
    final InvalidInstructionException expected = new InvalidInstructionException();
    final List<String> instructions = new ArrayList<>();
    instructions.add("LDR R2 R0 -65536");
    instructions.add("RET R2");
    Machine m = new Machine();
    try {
      m.execute(instructions);
    }
    catch (InvalidInstructionException e){
      assertSame(expected.toString(), e.toString());
      return;
    }
    assertEquals(expected.toString(), m.execute(instructions));
  }

  //To test B7 in Machine.
  @Test public void b7Test()
  {
    final int expected = 0;
    final List<String> instructions = new ArrayList<>();
    instructions.add("LDR R2 R0 1");
    instructions.add("RET R2");
    Machine m = new Machine();
    assertEquals(expected, m.execute(instructions));
  }

  //To test B8 in Machine.
  @Test public void b8Test()
  {
    final InvalidInstructionException expected = new InvalidInstructionException();
    final List<String> instructions = new ArrayList<>();
    instructions.add("LDR R2 R0 65536");
    instructions.add("RET R2");
    Machine m = new Machine();
    try {
      m.execute(instructions);
    }
    catch (InvalidInstructionException e){
      assertSame(expected.toString(), e.toString());
      return;
    }
    assertEquals(expected.toString(), m.execute(instructions));
  }

  //To test B9 in Machine.
  @Test public void b9Test()
  {
    final InvalidInstructionException expected = new InvalidInstructionException();
    final List<String> instructions = new ArrayList<>();
    instructions.add("MOV R2 -65536");
    instructions.add("RET R2");
    Machine m = new Machine();
    try {
      m.execute(instructions);
    }
    catch (InvalidInstructionException e){
      assertSame(expected.toString(), e.toString());
      return;
    }
    assertEquals(expected.toString(), m.execute(instructions));
  }

  //To test B10 in Machine.
  @Test public void b10Test()
  {
    final int expected = 1;
    final List<String> instructions = new ArrayList<>();
    instructions.add("MOV R2 1");
    instructions.add("RET R2");
    Machine m = new Machine();
    assertEquals(expected, m.execute(instructions));
  }

  //To test B11 in Machine.
  @Test public void b11Test()
  {
    final InvalidInstructionException expected = new InvalidInstructionException();
    final List<String> instructions = new ArrayList<>();
    instructions.add("MOV R2 65536");
    instructions.add("RET R2");
    Machine m = new Machine();
    try {
      m.execute(instructions);
    }
    catch (InvalidInstructionException e){
      assertSame(expected.toString(), e.toString());
      return;
    }
    assertEquals(expected.toString(), m.execute(instructions));
  }
}
