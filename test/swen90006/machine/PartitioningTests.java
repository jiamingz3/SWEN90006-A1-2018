package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;

import static org.junit.Assert.*;

public class PartitioningTests
{
  //Any method annotated with "@Before" will be executed before each test,
  //allowing the tester to set up some shared resources.
  @Before public void setUp()
  {
  }

  //Any method annotated with "@After" will be executed after each test,
  //allowing the tester to release any shared resources used in the setup.
  @After public void tearDown()
  {
  }

  //To test EC5 in Machine.
  @Test public void ec5Test()
  {
    final int expected = 0;
    final List<String> instructions = new ArrayList<>();
    instructions.add("RET R0 R1");
    Machine m = new Machine();
    assertEquals(expected, m.execute(instructions));
  }

  //To test EC6 in Machine.
  @Test public void ec6Test()
  {
    final NoReturnValueException expected = new NoReturnValueException();
    final List<String> instructions = new ArrayList<>();
    instructions.add("MOV R0 1");
    instructions.add("MOV R1 1");
    instructions.add("ADD R2 R0 R1");
    Machine m = new Machine();
    try {
        m.execute(instructions);
    }
    catch (NoReturnValueException e){
        assertSame(expected.toString(), e.toString());
      return;
    }
    assertEquals(expected.toString(), m.execute(instructions));
  }

  //To test EC7 in Machine.
  @Test public void ec7Test()
  {
    final int expected = 0;
    final List<String> instructions = new ArrayList<>();
    instructions.add("ADD R2 R0 R1");
    instructions.add("RET R2");
    Machine m = new Machine();
    assertEquals(expected, m.execute(instructions));
  }

  //To test EC8 in Machine.
  @Test public void ec8Test()
  {
    final InvalidInstructionException expected = new InvalidInstructionException();
    final List<String> instructions = new ArrayList<>();
    instructions.add("ADD R2 R0 R32");
    instructions.add("RET R2");
    Machine m = new Machine();
      try {
          m.execute(instructions);
      }
      catch (InvalidInstructionException e){
          assertSame(expected.toString(), e.toString());
          return;
      }
      assertEquals(expected.toString(), m.execute(instructions));
  }

  //To test EC9 in Machine.
  @Test public void ec9Test()
  {
    final InvalidInstructionException expected = new InvalidInstructionException();
    final List<String> instructions = new ArrayList<>();
    instructions.add("ADD R2 R0");
    instructions.add("RET R2");
    Machine m = new Machine();
      try {
          m.execute(instructions);
      }
      catch (InvalidInstructionException e){
          assertSame(expected.toString(), e.toString());
        return;
      }
    assertEquals(expected.toString(), m.execute(instructions));
  }

  //To test EC11 in Machine.
  @Test public void ec11Test()
  {
    final InvalidInstructionException expected = new InvalidInstructionException();
    final List<String> instructions = new ArrayList<>();
    instructions.add("LDR R2 R0 -65536");
    instructions.add("RET R2");
    Machine m = new Machine();
      try {
         m.execute(instructions);
      }
      catch (InvalidInstructionException e){
          assertSame(expected.toString(), e.toString());
        return;
      }
    assertEquals(expected.toString(), m.execute(instructions));
  }

  //To test EC13 in Machine.
  @Test public void ec13Test()
  {
      final InvalidInstructionException expected = new InvalidInstructionException();
    final List<String> instructions = new ArrayList<>();
    instructions.add("LDR R2 R0 65536");
    instructions.add("RET R2");
    Machine m = new Machine();
      try {
          m.execute(instructions);
      }
      catch (InvalidInstructionException e){
          assertSame(expected.toString(), e.toString());
        return;
      }
    assertEquals(expected.toString(), m.execute(instructions));
  }

  //To test EC18 in Machine.
  @Test public void ec18Test()
  {
      final InvalidInstructionException expected = new InvalidInstructionException();
    final List<String> instructions = new ArrayList<>();
    instructions.add("JMP -65536");
    instructions.add("RET R0");
    Machine m = new Machine();
      try {
          m.execute(instructions);
      }
      catch (InvalidInstructionException e){
          assertSame(expected.toString(), e.toString());
        return;
      }
    assertEquals(expected.toString(), m.execute(instructions));
  }

  //To test EC20 in Machine.
  @Test public void ec20Test()
  {
      final InvalidInstructionException expected = new InvalidInstructionException();
    final List<String> instructions = new ArrayList<>();
    instructions.add("JMP 65536");
    instructions.add("RET R0");
    Machine m = new Machine();
      try {
          m.execute(instructions);
      }
      catch (InvalidInstructionException e){
          assertSame(expected.toString(), e.toString());
        return;
      }
    assertEquals(expected.toString(), m.execute(instructions));
  }

  //To test EC21 in Machine.
  @Test public void ec21Test()
  {
      final InvalidInstructionException expected = new InvalidInstructionException();
    final List<String> instructions = new ArrayList<>();
    instructions.add("JMP abc");
    instructions.add("RET R0");
    Machine m = new Machine();
      try {
          m.execute(instructions);
      }
      catch (InvalidInstructionException e){
          assertSame(expected.toString(), e.toString());
        return;
      }
    assertEquals(expected.toString(), m.execute(instructions));
  }

  //To test EC22 in Machine.
  @Test public void ec22Test()
  {
    final int expected = 0;
    final List<String> instructions = new ArrayList<>();
    instructions.add("RET R0");
    instructions.add("RET R1");
    Machine m = new Machine();
    assertEquals(expected, m.execute(instructions));
  }

  //To test EC23 in Machine.
  @Test public void ec23Test()
  {
      final InvalidInstructionException expected = new InvalidInstructionException();
    final List<String> instructions = new ArrayList<>();
    instructions.add("R0");
    Machine m = new Machine();
      try {
          m.execute(instructions);
      }
      catch (InvalidInstructionException e){
          assertSame(expected.toString(), e.toString());
        return;
      }
    assertEquals(expected.toString(), m.execute(instructions));
  }

}
